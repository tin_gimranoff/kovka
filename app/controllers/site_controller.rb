#coding: utf-8
class SiteController < ApplicationController

	before_filter :construct
	add_breadcrumb "ГЛАВНАЯ", :root_path

	def index
		@paetitle = 'ГЛАВНАЯ'

	end

	def news
		@pagetitle = 'НОВОСТИ'
		add_breadcrumb @pagetitle, '/news'
		if params[:id]
			news = News.find(params[:id])
			@content = news.content
			@pagetitle = news.name
			add_breadcrumb @pagetitle, '/news/'+news.id.to_s
			render :textpage
		else
			@news = News.paginate(:page => params[:page], :per_page => 6)
			render :news
		end
	end

	def textpage
		page = Textpage.where(alias: params[:slug])
		if page
			@pagetitle = page[0].title
			@content = page[0].content
			add_breadcrumb @pagetitle, '/'+page[0].alias
			render :textpage
		else
			render '404'
		end
	end

	def works
		@pagetitle = 'НАШИ РАБОТЫ'
		add_breadcrumb @pagetitle, '/works'
		if params[:alias]
			category = Category.where(alias: params[:alias])[0]
			@albums = Album.where(categorie_id: category.id).paginate(:page => params[:page], :per_page => 16)
			@pagetitle = category.name
			add_breadcrumb category.name, '/works/'+category.alias
		else
			@albums = Album.paginate(:page => params[:page], :per_page => 16)
		end
	end

	def vacancy 
		@vacancys = Vacancy.order("id DESC")
		@pagetitle = 'ВАКАНСИИ'
		add_breadcrumb @pagetitle, '/vacancy'
	end

	def contacts
		@pagetitle = 'КОНТАКТЫ'
		add_breadcrumb 'КОНТАКТЫ', '/contacts'
		if params[:send_mail]
			@errors = Array.new
			if params[:email].blank? || params[:email].scan(/([a-z0-9_.-]+)@([a-z0-9-]+)\.[a-z.]+/i).size == 0
				@errors << "Не верно заполнено поле Email"
				return
			end
			if params[:message].blank?
				@errors << "Не заполнено поле Сообщение"
				return
			end
			SiteMailer.getcall_email(params[:email], params[:messsage]).deliver
			@success = 1
		end
	end

	def sitemap
		respond_to do |format|
			format.html {
				@pagetitle = 'КАРТА САЙТА'
				add_breadcrumb @pagetitle, '/sitemap'
				@urls = Array.new
				@pagetitle = 'КАРТА САЙТА'
				url = '/'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Hash.new})

				url = '/news'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				news = News.all
				news.each do |n|
					page_info = Seo.where(url: url+'/'+n.id.to_s)
					if !page_info[0]
						page_title = url+'/'+n.id.to_s
					else
						page_title = page_info[0].title
					end
					@urls[1][:childs].push({title: page_title, url: url+'/'+n.id.to_s, childs: Array.new})
				end

				url = '/works'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				url = '/vacancy'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				url = '/contacts'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				texages = Textpage.all
				texages.each do |n|
					page_info = Seo.where(url: '/'+n.alias.to_s)
					if !page_info[0]
						page_title = '/'+n.alias.to_s
					else
						page_title = page_info[0].title
					end
					@urls.push({title: page_title, url: '/'+n.alias.to_s, childs: Array.new})
				end

			}

			format.xml {
				headers['Cntent-Type'] = "aplication/xml"
				render :layout => false
			}
		end
	end

	private
		def construct

			page_info = Seo.where(url: request.path)[0]
			if !page_info.blank?
				@title = page_info.title
				@keywords = page_info.keywords
				@description = page_info.description
			end

			@slider = Slider.all
			@news_widget = News.limit(3)

			if params[:getcall]
	          @getcall = Getcall.new(params[:getcall])
	          if @getcall.valid?
	             SiteMailer.get_specialist(@getcall.name, @getcall.city, @getcall.phone, @getcall.email, @getcall.message).deliver
	          end
	        else
	           @getcall = Getcall.new
	        end

	        @resent_works = Album.order("id DESC").limit(8)
		end
end