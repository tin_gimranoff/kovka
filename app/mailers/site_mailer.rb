#coding: utf-8
class SiteMailer < ActionMailer::Base
  default from: "robot@uzor.pro"

  def getcall_email(email, message)
  	@email = email
  	@message = message
  	mail(to: 'edvin@acond.ru', subject: 'Заявка на обратный звонок') do |format|
      format.text { render "getcall_email" }
    end
  end

  def get_specialist(name, city, phone, email, message)
  	@email = email
  	@message = message
  	@name = name
  	@city = city
  	@phone = phone
  	mail(to: 'edvin@acond.ru', subject: 'Вызов специалиста') do |format|
      format.text { render "get_specialist" }
    end
  end
end
