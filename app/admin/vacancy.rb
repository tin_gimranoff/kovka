#coding: utf-8
ActiveAdmin.register Vacancy do

  menu label: "Вакансии"

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :title, :city, :req, :price
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая вакансия" do
        f.input :title
        f.input :city
        f.input :req
        f.input :price
      end
      f.actions
  end

end
