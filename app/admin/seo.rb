#coding: utf-8
ActiveAdmin.register Seo do

  menu label: "СЕО"
  permit_params :url, :title, :keywords, :description
  index do
    selectable_column
    id_column
    column :url
    column :title
    column :keywords
    column :description
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая запись" do
        f.input :url
        f.input :title
        f.input :keywords
        f.input :description
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
