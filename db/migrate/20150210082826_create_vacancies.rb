class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :title
      t.string :city
      t.string :req
      t.string :price

      t.timestamps
    end
  end
end
