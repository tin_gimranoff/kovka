class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.has_attached_file :image
      t.string :name
      t.text :intro
      t.text :content

      t.timestamps
    end
  end
end
