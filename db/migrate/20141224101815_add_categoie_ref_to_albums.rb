class AddCategoieRefToAlbums < ActiveRecord::Migration
  def change
    add_reference :albums, :categorie, index: true
  end
end
